package oop;

public class Home {
    boolean hasOpenWindow = false;
    static boolean hasOpenDor = false;

    public Home() {
    }

    public void openWindow() {
        hasOpenWindow = true;
    }

    public static void openDor() {
        hasOpenDor = true;
    }

    public void printState() {
        System.out.println("Stan okien: " + hasOpenWindow);
        System.out.println("Stan drzwi: " + hasOpenDor);
    }
}
