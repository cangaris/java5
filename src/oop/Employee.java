package oop;

class Employee {
    private String firstname;
    private String lastname;
    private int salary;
    private String companyName = "WebCoders";

    Employee(String firstname, String lastname, int salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        if (salary > 0) {
            this.salary = salary;
        }
    }

    void addSalary(int addValue) {
        if (addValue > 0) {
            salary = salary + addValue;
        }
    }

    void setSalary(int salary) {
        if (salary > 0) {
            this.salary = salary;
        }
    }

    void print() {
        System.out.println(firstname);
        System.out.println(lastname);
        System.out.println(salary);
        System.out.println("----");
    }
}
