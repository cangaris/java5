package oop;

public class Main2 {
    public static void main(String[] args) {

        // a5 = 90

        Employee e1 = new Employee("Jan", "Kiepski", 2500); // a1 -> a5

        Employee e2 = new Employee("Kasia", "Nowak", 4500); // a2 -> a5
        e2.addSalary(1000);

        Employee e3 = new Employee("Ania", "Kowalska", 14500); // a3 -> a5

        e3.setSalary(-1000);

        e1.print();
        e2.print();
        e3.print();
    }
}
