package oop;

public class Main {
    public static void main(String[] args) {

        Home home1 = new Home();
        home1.openWindow();

        Home home2 = new Home();
        home2.openWindow();

        Home.openDor();

        home1.printState();
        System.out.println("-----");
        home2.printState();

        Integer integer = 1; // new Integer(1);
        integer.intValue();

        String productName = "Damian"; // new String("dasfdsfds");
        productName.toUpperCase();
        productName.toLowerCase();
        productName.startsWith("Dam");

        Home.openDor();
    }
}
