package arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.stream.Collectors;

class Home {
    private String member;
    Home(String member) {
        this.member = member;
    }

    @Override
    public int hashCode() {
        return member.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return hashCode() == obj.hashCode();
    }

    @Override
    public String toString() {
        return "Home{" +
            "member='" + member + "'" +
            '}';
    }
}

public class Main {
    public static void main(String[] args) {

        /**
         * [3][6][5][5][3][x][]
         * [][][][][][][x]
         * [][][x][][][][]
         * var arr = [3,6,5,5,3] -> arr[3] -> 2
         * odnajdowanie wartości po indexie (+)
         * edycja po indexie (+)
         * dodawanie nowych elementów (-)
         * usuwanie elementów po indexie (-)
         */
        var arrayList = new ArrayList<String>(); // array?
        arrayList.add("Marcin"); // 0
        arrayList.add("Adela"); // 1
        arrayList.add("Marek"); // 2
        arrayList.add("Magda");

        // Function<String, Apple> stringToApple = (String string) -> new Apple(string); // arrayList.stream().map(Apple::new);

        var womanWithHomes = arrayList.stream()
            .filter(name -> !name.endsWith("a"))
            .map( Home::new )
            .collect(Collectors.toList()); // .toList()

        womanWithHomes.add(new Home("Jan"));
        womanWithHomes.forEach(System.out::println);

        // arrayList.forEach(element -> System.out.println(element)); // System.out::println
        // System.out.println(arrayList.get(2)); // Marek

        /**
         * [3@6][6@5][5@3][][3@9][x][9@0]
         * [0@9][x][9@4][x][x][4][x]
         * [][][x][][][][]
         * var arr = [3,6,5,3,9,0,9,4] -> arr[3] -> 2
         * odnajdowanie wartości po indexie (-)
         * edycja po indexie (-)
         * dodawanie nowych elementów (+)
         * usuwanie elementów po indexie (+)
         */
        var linkedList = new LinkedList<>(); // link?
        // System.out.println(linkedList.size());

        var hashSet = new HashSet<Home>();
        var apple1 = new Home("");
        hashSet.add(apple1);

        var apple2 = new Home("x");
        hashSet.add(apple2);

        // System.out.println(hashSet.contains(new Apple("x")));

        var apple3 = new Home("x");
        hashSet.add(apple3);


//        System.out.println(apple1);
//        System.out.println(apple2);
//        System.out.println(apple3);
//        System.out.println(hashSet.size());


        /**
         * const user = {
         *     firstName: "Jan",
         *     lastName: "Kowalski",
         *     phone: "+77 545 746 876"
         * }
         * user['firstName']
         */
        var map = new HashMap<String, String>();
        map.put("firstName", "Jan");
        map.put("lastName", "Kowalski");
        map.put("phone", "+77 545 746 876");

        // map.remove("phone");

        // System.out.println(map.get("phone"));
    }
}
