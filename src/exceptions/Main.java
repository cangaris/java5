package exceptions;

public class Main {
    public static void main(String[] args) {
        try {
            throw new AccessDeniedException("hello");
        } catch (AccessDeniedException e) {
            System.out.println(e.getMessage());
        }
    }
}
