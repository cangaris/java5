package exceptions;

class AccessDeniedException extends RuntimeException {
    AccessDeniedException(String message) {
        super(message);
    }
    AccessDeniedException() {
        super("AccessDeniedException");
    }
}
