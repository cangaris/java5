package streams;

import java.util.List;

public class Main1 {
    public static void main(String[] args) {

         var games = List.of(
            new BoardGame("Terraforming Mars", 8.38, 123.49, 1, 5),
            new BoardGame("Codenames", 7.82, 64.95, 2, 8),
            new BoardGame("Puerto Rico", 8.07, 149.99, 2, 5),
            new BoardGame("Terra Mystica", 8.26, 252.99, 2, 5),
            new BoardGame("Scythe", 8.3, 314.95, 1, 5),
            new BoardGame("Power Grid", 7.92, 145, 2, 6),
            new BoardGame("7 Wonders Duel", 8.15, 109.95, 2, 2),
            new BoardGame("Dominion: Intrigue", 7.77, 159.95, 2, 4),
            new BoardGame("Patchwork", 7.77, 75, 2, 2),
            new BoardGame("The Castles of Burgundy", 8.12, 129.95, 2, 4)
        );

        var filteredGames = games.stream()
            .filter(boardGame -> boardGame.price < 150)
            .filter(boardGame -> boardGame.minPlayers > 1)
            .filter(boardGame -> boardGame.rating > 8)
            .toList();

        /*
        Zadanie: wyfiltrować listę spełniając poniższe kryteria:
        1. cena < 150
        2. multiplayer (min 2 graczy)
        3. rating > 8
         */
        System.out.println(games);
        System.out.println(filteredGames);
    }
}
