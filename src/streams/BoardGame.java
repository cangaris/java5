package streams;

class BoardGame {
    public final String name; // Diablo :)
    public final double rating; // 5 / 5
    public final double price; // 100 zł, // BigDecimal
    public final int minPlayers; // 1
    public final int maxPlayers; // 8

    public BoardGame(String name, double rating, double price, int minPlayers, int maxPlayers) {
        this.name = name;
        this.rating = rating;
        this.price = price;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }

    @Override
    public String toString() {
        return "BoardGame{" +
            "name='" + name + "'" +
            ", rating=" + rating +
            ", price=" + price +
            ", minPlayers=" + minPlayers +
            ", maxPlayers=" + maxPlayers +
            '}';
    }
}
