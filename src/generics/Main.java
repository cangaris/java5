package generics;

abstract class Fruit {
    abstract void getFruitName();
}

class Strawberry extends Fruit {
    @Override
    void getFruitName() {
        System.out.println("Strawberry");
    }
}

class Apple extends Fruit {
    @Override
    void getFruitName() {
        System.out.println("Apple");
    }
}

class Orange extends Fruit {
    @Override
    void getFruitName() {
        System.out.println("Orange");
    }
}

class Mango extends Fruit {
    @Override
    void getFruitName() {
        System.out.println("Mango");
    }
}

class Box<T extends Fruit> {
    private T element;
    Box(T element) {
        this.element = element;
    }
    public T getElement() {
        return element;
    }
}

public class Main {
    public static void main(String[] args) {

        var apple = new Apple();
        var orange = new Orange();
        var strawberry = new Strawberry();

        var appleBox = new Box<Apple>(apple);
        var orangeBox = new Box<>(orange);
        var strawberryBox = new Box<>(strawberry);
        var mangoBox = new Box<>(new Mango());

        method1(appleBox);
        method1(orangeBox);
        method1(strawberryBox);
        method1(mangoBox);
    }

    private static void method1(Box<? extends Fruit> box) {
        box.getElement().getFruitName();
    }
}
