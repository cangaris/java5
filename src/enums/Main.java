package enums;

public class Main {
    public static void main(String[] args) {
        var gender = Gender.FEMALE;
        var genderStr = GenderClass.FEMALE;

        gender = Gender.MALE;
        genderStr = "fdkshfkjdsh";

        var tshirtSize = TshirtSize.L;
        System.out.println(tshirtSize.name()); // "L"
        System.out.println(tshirtSize.getChestWidth());

        var size = "M";
        var value = TshirtSize.valueOf(size).getShirtLength();
        System.out.println(value);
    }
}
