package extend;

public abstract class Animal {

    private String name;

    Animal(String name) {
        this.name = name;
    }

    void go() {
        System.out.println("go");
    }
}
