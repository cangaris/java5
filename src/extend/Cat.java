package extend;

public class Cat extends Animal {

    Cat(String name) {
        super(name);
        super.go();
    }

    void wspinajSieNaDrzewo() {
    }

    void mialcz() {
    }
}
