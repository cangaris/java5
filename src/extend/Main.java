package extend;

public class Main {
    public static void main(String[] args) {
//        Animal animal = new Animal("Zwierzak");
//        animal.go();

        Cat cat = new Cat("Miałczek");
        cat.go();

        Dog dog = new Dog("Szczekacz");
        dog.go();

        Rabbit rabbit = new Rabbit("Skoczek");
        rabbit.go();

        MyString2 my = new MyString2();
        my.test();

        Animal test = new Eagle("");
    }
}
