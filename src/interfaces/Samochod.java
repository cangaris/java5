package interfaces;

public class Samochod implements Pojazd {

    @Override
    public void jedz() {
        System.out.println("Samochod jedzie");
    }

    @Override
    public void stop() {
    }

    @Override
    public void doladuj() {
        System.out.println("doladuj Samochod");
    }
}
