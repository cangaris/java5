package interfaces;

public class Samolot implements Lataj, Stop, Jedz, Doladuj {

    @Override
    public void doladuj() {
    }

    @Override
    public void jedz() {
    }

    @Override
    public void lataj() {
    }

    @Override
    public void stop() {
    }

    protected void test() {
    }
}
