package interfaces;

public interface Pojazd extends Jedz, Stop, Doladuj {

    int ILOSC_KOL = 4;

    default void test1() {
        testPrivate();
    }

    default void test2() {
        testPrivate();
    }

    private void testPrivate() {
        System.out.println("hello"); // Don't Repeat Yourself (DRY)
    }
}
