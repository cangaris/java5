import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner inputScanner = new Scanner(System.in);
        List allowedOptions = List.of("A", "B", "C");

        System.out.println("-------");
        System.out.println("Menu:");
        System.out.println("[A] - dodawanie");
        System.out.println("[B] - odejmowanie");
        System.out.println("[C] - dzielenie");
        System.out.println("-------");

        System.out.print("Wybierz opcję: ");
        String chosenOption = inputScanner.next();

        if (!allowedOptions.contains(chosenOption)) {
            System.out.print("Wybrano niedozwoloną opcję");
            return;
        }

        int x = getParameter(inputScanner);
        int y = getParameter(inputScanner);

        float z = 0f;
        switch (chosenOption) {
            case "A" -> z = plus(x, y);
            case "B" -> z = minus(x, y);
            case "C" -> {
                try {
                    z = divide(x, y);
                } catch (ArithmeticException e) {
                    System.out.println(e.getMessage());
                    return;
                }
            }
        }
        System.out.println("Wynik: " + z);
    }

    static float plus(float x, float y) {
        return x + y;
    }

    static float minus(float x, float y) {
        return x - y;
    }
    static Float divide(float x, float y) {
        if (y == 0) {
            throw new ArithmeticException("Niedozwolone dzielenie przez zero");
        }
        return x / y;
    }

    static int getParameter(Scanner inputScanner) {
        System.out.print("Wpisz liczbę: ");
        try {
            return inputScanner.nextInt();
        } catch (Exception e) {
            return getParameter(new Scanner(System.in));
        }
    }
}
