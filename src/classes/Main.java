package classes;

public class Main {
    public static void main(String[] args) {
        new Apple();
    }

    public static class Apple {
    }

    public void test() {
        String[] array = new String[] {"Damian", "Kowalski"};
        class Test {
            String firstName;
            String lastName;
            Test(String[] array) {
                firstName = array[0];
                lastName = array[1];
            }
        }
        var x = new Test(array);
    }
}
