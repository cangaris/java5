package gettersSetters;

class User {
    private int age;

    User(int age) {
        setAge(age);
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        } else {
            throw new IllegalArgumentException("Wiek nie może być mniejszy niż zero!");
        }
    }

    public int getAge() {
        return age;
    }
}

public class Main {
    public static void main(String[] args) {

        var a = 1;
        var b = a;
        b = 2;
        System.out.println(a); // 1
        System.out.println(b); // 2

        var user = new User(10); // z15

        System.out.println(user);

        var user2 = new User(10); // u4

        System.out.println(user2);
        System.out.println(user.equals(user2)); // false

        var user3 = user2;
        System.out.println(user3); // u4
        System.out.println(user2.equals(user3)); // true

        user3.setAge(30);

        System.out.println(user.getAge());
        System.out.println(user2.getAge());
        System.out.println(user3.getAge());

        // -------

        String x1 = new String("x"); // a1
        String x2 = new String("x"); // b8

        String x3 = "x"; // <- string pool ('x' -> h4)
        String x4 = "x"; // o5

        System.out.println(x1 == x2);
        System.out.println(x2 == x3);
        System.out.println(x3 == x4);
        System.out.println(x4 == x1);
    }
}
