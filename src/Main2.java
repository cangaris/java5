// import oop.Employee;

public class Main2 {

    public static void main(String... args) {

        char[] name = {'d', 'a', 'm', 'i', 'a', 'n'}; // length = 6

        for (int i = name.length - 1; i >= 0; i--) {
            System.out.print(name[i]);
        }

        for (char letter : name) {
            System.out.print(letter);
        }

        int i = 0;
        while (i < 10) {
            i++;
            if (i % 2 == 0) {
                continue; // break;
            }
            System.out.println(i);
        }

        // new Employee("", "", 0);
    }
}
