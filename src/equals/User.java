package equals;

public class User {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;

    public User(Integer id,String firstName, String lastName, Integer age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        var user = (User) obj;
        return user.id.equals(id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
