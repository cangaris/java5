package equals;

public class Main {
    public static void main(String[] args) {

        var user1 = new User(1, "Damian", "Kowalski", 10);
        var user2 = new User(1, "Jan", "Nowak", 20);

        System.out.println(user1);
        System.out.println(user2);
        System.out.println(user1.equals(user2));

    }
}
